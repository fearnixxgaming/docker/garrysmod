#!/bin/bash
cd /home/container || exit 1

# Make internal Docker IP address available to processes.
export INTERNAL_IP=$(ip route get 1 | awk '{print $NF;exit}')

# Update Source Server
echo "SRCDS_APPID ${SRCDS_APPID}"
if [ ! -z "${SRCDS_APPID}" ]; then
    ./steamcmd/steamcmd.sh +login anonymous +force_install_dir /home/container +app_update "${SRCDS_APPID}" +quit
fi

BASEDIR=$(pwd)
REPOS=()
readarray REPOS <<< "$(find /home/container -type f -name .gitrepo)"
echo "Repo STR: ${REPOS[*]}"
if [ ${#REPOS[@]} -gt 0 ]; then
	mkdir -p tmp
	echo "Found ${#REPOS[@]} git repositories"
	for REPO in "${REPOS[@]}"
	do
		DIR=$(dirname "$REPO")
		REPO_DETAIL=()
		IFS=" " read -r -a REPO_DETAIL <<< "$(cat $REPO)"
		REPO_URL=${REPO_DETAIL[0]}
		cd "$DIR" || exit 1
		if [ ! -d .git ]; then
			echo "Git repo below $DIR not found. Cloning..."
			git clone --recursive "$REPO_URL" "$BASEDIR/tmp" || exit 1
			mv $BASEDIR/tmp/* $DIR/
			mv $BASEDIR/tmp/.* $DIR/
		fi 
		if [ -f .update_always ] || [ -f .update_once ]; then
			echo "Updating repository..."
			git pull
			if [ -e .update_once ]; then
				rm .update_once
			fi
		fi
	done
	if [ -e "$BASEDIR/tmp" ]; then
		rm -rf "$BASEDIR/tmp"
	fi
	cd "$BASEDIR" || exit 1
fi

if [[ ! -z "$SERVER_PRESET" ]]; then
    if [[ ! -e /home/container/preset ]]; then
        git clone $SERVER_PRESET /home/container/preset
    else
        cd /home/container/preset
        git pull
        cd /home/container
    fi
    if ! /home/container/preset/apply.sh; then
        echo "Could not apply preset!"
        exit 1
    fi
fi

# Fix many scripts not correctly setting file permissions in Git and archives.
echo "Fixing script executable-permissions"
chmod -R u+x /home/container

# Replace Startup Variables
MODIFIED_STARTUP=$(eval echo $(echo ${STARTUP} | sed -e 's/{{/${/g' -e 's/}}/}/g'))
echo ":$(pwd)$ ${MODIFIED_STARTUP}"

# Run the Server
eval ${MODIFIED_STARTUP}
